
  import {useState} from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import {Container} from "react-bootstrap";
import { UserProvider } from "./UserContext";
import AppNavbar from "./components/AppNavbar";


import Error from "./pages/Error";
import Home from "./pages/Home";
import AdminDashboard from "./pages/AdminDashboard"
import AddProduct from "./pages/AddProduct"
import Orders from './pages/Orders';
import Cart from './pages/Cart';
import UsersList from "./pages/UsersList"
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";





import './App.css';


function App() {

  const [user, setUser] = useState({
            //null
    // email: localStorage.getItem("email")
    id: null,
    isAdmin: null
  })

         //Function for clearing localStorage on logout
  const unsetUser = () =>{
    localStorage.clear();
  }

 return (


    <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar />
          <Container fluid>
              <Routes>
               <Route exact path ="/" element={<Home />} />
                  <Route exact path ="/register" element={<Register />} />
                   <Route exact path ="/admin" element ={< AdminDashboard />}/>
                   <Route exact path ="/products" element ={< Products />}/>  
                   <Route exact path ="/carts" element ={< Cart />}/>                 
                   <Route exact path ="/addProduct" element ={< AddProduct />}/>
                   <Route exact path ="/allUsers" element ={< UsersList />}/>
                   <Route exact path ="/orders" element ={< Orders />}/>
                    <Route exact path ="/products/:productId" element ={< ProductView />}/>
                  <Route exact path ="/login" element={<Login />} />
                  <Route exact path ="/logout" element={<Logout />} />
                  <Route exact path ="*" element={<Error />} />
              </Routes>
          </Container>
        </Router>
    </UserProvider>
  );
}


export default App;

