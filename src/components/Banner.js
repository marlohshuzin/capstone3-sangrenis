import {Row, Col} from "react-bootstrap";


export default function Banner({data}){
	
	const {title, content } = data;

	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>{title}</h1>
				<p>{content}</p>
				
			</Col>
		</Row>
	)
}