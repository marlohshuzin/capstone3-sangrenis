import Carousel from 'react-bootstrap/Carousel';

export default function CarouselSec() {
    return (
        <Carousel variant="light" controls={false} fade>
            <Carousel.Item className="carouselFit" interval={1500}>
                <img
                    className="d-block w-100 h-100 carouselImg"
                    src={require('./imgCarousel1.jpg')}
                    alt="First slide"
                />
                <div className="overlay">
                    <h1> Classic Skate Shoe </h1>
                    <h5>Old Skool</h5>

                </div>
            </Carousel.Item>            
            <Carousel.Item className="carouselFit" interval={1500}>
                <img
                    className="d-block w-100 h-100 carouselImg"
                    src={require('./imgCarousel2.jpg')}
                    alt="First slide"
                />
                <div className="overlay">
                    <h1>Comfortable and Durable</h1>
                    <h5>Ultraboost</h5>

                </div>
            </Carousel.Item>            
            <Carousel.Item className="carouselFit" interval={1500}>
                <img
                    className="d-block w-100 h-100 carouselImg"
                    src={require('./imgCarousel3.jpg')}
                    alt="First slide"
                />
                <div className="overlay">
                    <h1>Lightweight Agility</h1>
                    <h5>Mamba</h5>
                </div>
            </Carousel.Item>
        </Carousel>
    )
}