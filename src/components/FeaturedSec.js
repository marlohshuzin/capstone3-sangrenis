
import { Row, Col } from 'react-bootstrap';


export default function CarouselSection() {
    return (
        <div className="my-5">
            <Row>
                <Col xs={12} md={7} className="p-0">
                    <img
                        className="img-fluid"
                        src={require('./imgFeatured1.jpg')}
                        alt="First slide"
                    />
                </Col>
                <Col xs={12} md={5}>
                    <div className="d-flex flex-column align-items-center justify-content-center p-3 mt-xl-3">
                        <h4>Basketball Shoes</h4>
                        <p className="w-lg-75 gray">
                         is an American multinational corporation that is engaged in the design, development, manufacturing, and worldwide marketing and sales of footwear
                        </p>

                    </div>
                </Col>
            </Row>
            <Row>
                <Col xs={12} md={7} className="p-0 order-lg-2">
                    <img
                        className="img-fluid"
                        src={require('./imgFeatured2.jpg')}
                        alt="First slide"
                    />
                </Col>
                <Col xs={12} md={5}>
                    <div className="d-flex flex-column align-items-center justify-content-center p-3 mt-xl-3 order-lg-1">
                        <h4>Soccer Shoes</h4>
                        <p className="w-lg-75 gray">
                            Express your creativity through our adidas originals shoes and footwear, a perfect fusion of sport and style. Buy online today, delivered to your door.
                        </p>

                    </div>
                </Col>
            </Row>            
            <Row>
                <Col xs={12} md={7} className="p-0">
                    <img
                        className="img-fluid"
                        src={require('./imgFeatured3.jpg')}
                        alt="First slide"
                    />
                </Col>
                <Col xs={12} md={5}>
                    <div className="d-flex flex-column align-items-center justify-content-center p-3 mt-xl-3">
                        <h4>Hiking Shoes</h4>
                        <p className="w-lg-75 gray">
                            provide your foot with support and protection from rocks, as well as give you traction on dry and wet surfaces
                        </p>
                    </div>
                </Col>
            </Row>
        </div>
    )
}