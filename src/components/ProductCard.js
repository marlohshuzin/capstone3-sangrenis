import { Link } from "react-router-dom";
import { Card, Button, Col, Row} from "react-bootstrap";

//Deconstruct the "productProp" form the "props" object to shorten syntax.
export default function ProductCard({productProp}){

const {_id, name, description, price, quantity} = productProp;
return (

	<Row className="mt-3 mb-3">
	  <Col>
		<Card className="cardHighlight p-3">
		    <Card.Body>
		        <Card.Title>
		            {name}
		        </Card.Title>
		        <Card.Subtitle>Description: </Card.Subtitle>
		        <Card.Text>
		            {description}
		        </Card.Text>
		        <Card.Subtitle>${price}</Card.Subtitle>
		        <Card.Text>
		            Quantity: {quantity}
		        </Card.Text>
		    	{/*We will be able to select a specific product through its url*/}
		        <Button as={Link} to={`/products/${_id}`} variant="primary">Order</Button>

		        {/*<Button variant="primary mx-1" onClick={enroll}>Order</Button>
		        <Button variant="danger mx-1" onClick={unEnroll}>Cancel</Button>*/}
		    </Card.Body>
		</Card>

	</Col>
	</Row>
	)
}