import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";
import {Navigate, useNavigate, Link} from "react-router-dom";

import Swal from "sweetalert2";
import {Button, Form} from "react-bootstrap";



export default function AddProduct(){


	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	//State hooks to store the values of the input fields
	const [name, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [quantity, setQuantity] = useState("");

	//Check if the values are successfully binded/passed.
	console.log(name);
	console.log(description);
	console.log(price);
	console.log(quantity);


	
	function addProduct(e){
		//prevents the page redirection via form submit
		e.preventDefault();

				fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
					method: "POST",
					headers:{
						"Content-Type": "application/json",
						Authorization: `Bearer ${localStorage.getItem("token")}`
					},
					body: JSON.stringify({
						name: name,
						description: description,
						price: price,
						quantity: quantity,
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data){
						//Clear input fields
						setProductName("");
						setDescription("");
						setPrice("");
						setQuantity("");

						Swal.fire({
							title: "Completed",
							icon: "success",
							text: "Product successfully added!"
						})

						
						navigate("/admin");
					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		

	//State to determine whether submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);

	// To enable the submit button:

	useEffect(()=>{
		if((name !== "" && description !== "" && price !== "" && quantity !== "")){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[name, description, price, quantity])



	return(
		(user.isAdmin)
		?
		<>
		<div className="mt-5 mb-3 text-center">
		<h1>Admin Dashboard</h1>
		{/*A button to add a new product*/}
		<Button as={Link} to="/addProduct" variant="primary" size="lg" className="mx-2" >Add Product</Button>
		<Button as={Link} to="/admin" variant="danger" size="lg" className="mx-2" >Products</Button>
		<Button as={Link} to="/allUsers" variant="secondary" size="lg" className="mx-2" >Users List</Button>
		<Button as={Link} to="/orders" variant="success" size="lg" className="mx-2" >Orders</Button>
    	</div>
			<Form  onSubmit = {(e) => addProduct(e)}>

				<Form.Group className="mb-3" controlId="name">
				  <Form.Label>Product Name</Form.Label>
				  <Form.Control type="text" placeholder="Product Name" value={name} onChange={e => setProductName(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="description">
				  <Form.Label>Product description</Form.Label>
				  <Form.Control type="text" placeholder="description" value={description} onChange={e => setDescription(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="price">
				  <Form.Label>Price</Form.Label>
				  <Form.Control type="number" placeholder="price" value={price} onChange={e => setPrice(e.target.value)}/>
				</Form.Group>

			      <Form.Group className="mb-3" controlId="quantity">
			        <Form.Label>Quantity</Form.Label>
			        <Form.Control type="quantity" placeholder="Quantity" value={quantity} onChange={e => setQuantity(e.target.value)}/>
			      </Form.Group>
		      
		      {
		      	isActive
		      	?
		      		<Button variant="primary" type="submit" id="submitBtn">
		      		  Submit
		      		</Button>
		      	:
		      		<Button variant="primary" type="submit" id="submitBtn" disabled>
		      		  Submit
		      		</Button>
		      }
		    </Form>
    </>

		:
		<Navigate to="/products" />
	)
}