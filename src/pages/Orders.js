import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";


export default function Orders(){

	// to validate the user role.
	const {user} = useContext(UserContext);

	const [allOrders, setAllOrders] = useState([]);

	//"fetchData()" wherein we can invoke if their is a certain change with the product.
	const fetchData = () =>{
		// Get all products in the database
		fetch(`${process.env.REACT_APP_API_URL}/orders/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllOrders(data.map(order => {
				return(
					<tr key={order._id}>
						<td>{order._id}</td>
						<td>{order.userId}</td>
						<td>{order.bill}</td>
						<td>{order.purchasedOn}</td>
					</tr>
				)
			}))

		})
	}


	//Making the user an admin
	

	// To fetch all products in the first render of the page.
	useEffect(()=>{
		// invoke fetchData() to get all products.
		fetchData();
	}, [])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				<Button as={Link} to="/addProduct" variant="primary" size="lg" className="mx-2" >Add Product</Button>
				<Button as={Link} to="/admin" variant="danger" size="lg" className="mx-2" >Products</Button>
				<Button as={Link} to="/allUsers" variant="secondary" size="lg" className="mx-2" >Users List</Button>
				<Button variant="success" size="lg" className="mx-2">Orders</Button>
			</div>
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         <th>Orders ID</th>
		         <th>Users</th>
		         <th>Bill</th>
		         <th>Date Ordered</th>
		       </tr>
		     </thead>
		     <tbody>
		       {allOrders}
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
	)
}
