import { useState, useEffect, useContext } from "react";
import { Link, useParams } from "react-router-dom";

import Swal from "sweetalert2";
import { Container, Card, Button, Row, Col } from "react-bootstrap";

import UserContext from "../UserContext";

export default function ProductView(){



	const increaseQuantity = () => {
		setQuantity(quantity + 1);
	}

	const decreaseQuantity = () => {
		if(quantity > 1) {
			setQuantity(quantity - 1);
		}
	}
	//To check if their is already a logged in user. Change the button from "Enroll" to "Login" if the user is not logged in.
	const { user } = useContext(UserContext);

	//Allow us to gain access to methods that will redirect a user to different page after enrolling a product.

	// "useParams" hook allows us to retrieve the productId passed via URL.
	const { productId } = useParams();

	// Create state hooks to capture the information of a specific product and display it in our application.
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const[quantity, setQuantity] = useState(0);


	const addToCart = (productId, quantity) => {
		fetch(`${process.env.REACT_APP_API_URL}/carts/`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity,
				
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				Swal.fire({
					  title: 'Sweet!',
					  text: `(${quantity}) ${name} added to cart`
					})

				
			}
			else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: ""
				})
			}
		})
	}

	useEffect(() =>{
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setQuantity(data.quantity)
		})

	}, [productId])

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>${price}</Card.Text>
						<Card.Title className="d-flex ">
							<button type="button" className="quantity-btn" onClick={increaseQuantity}>+</button>
							<p className="quantity-text">{quantity}</p>
							<button className="quantity-btn" onClick={decreaseQuantity}>-</button>
							</Card.Title>
							<div className="d-grid gap-2">
							{
								(user.id !== null)
								?
								<>
								<Button variant="primary" size="lg" onClick={() => addToCart(productId, quantity)}>Add to Cart</Button>
								<Button variant="danger" size="lg" as={Link} to={`/products`} >Cancel</Button>
								</>
								:
								<Button as={Link} to="/login" variant="primary" size="lg">Please Login </Button>
							}
							</div>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}