import {useEffect, useState, useContext} from "react";
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";

import ProductCard from "../components/ProductCard";

export default function Products(){

	//State tha will be used to store the products retrieved form the database
	 const [products, setProducts] = useState([]);

	 // To be used for validating the "role" of the user to login.
	 const {user} = useContext(UserContext);

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product =>{
				return(
					<ProductCard key={product._id} productProp={product} />
				)
			}))
		})
	},[])

	return(
		(user.isAdmin)
		?
			<Navigate to ="/admin" />
		:
		<>
			<h1 className="my-5 text-center">Products</h1>
			{products}
		</>
	)
}
